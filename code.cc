#include <SoftwareSerial.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <SoftwareSerial.h>

// initialize ultrason1
int pinTrig = 8;
int pinEcho = 9;
long temps;
float distance;
float newDistance;

// initialize ultrason2
int pinTrig2 = 11;
int pinEcho2 = 12;
long temps2;
float distance2;

// initialisation lcd
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

//initialisation lightsensor

const int lightsensorPin = 0;
const int ledPin = 10;
int lightVal;

//initialisation moteur

int pos = 0;

Servo servo_2;

//initialisation flamme
// Définition des broches utilisées
int capt_num = 22; // Interface Capteur Flamme
int capt_ana = A1; //  Interface Capteur Flamme
 
int val ;
float capteur_flamme;

//initialisation capteur de mouvement + temps

int MouvementPin = 13;  // choose the input pin (for PIR sensor)
int trashState=0;
unsigned long old_time_for_5min;
unsigned long old_time_for_20sec;
unsigned long difference;
unsigned long new_time;

//initialisation gsm

SoftwareSerial SIM900(30,28); //(tx,rx)
 


void setup() 
{

  //gsm

SIM900.begin(19200);
delay(20000);


  //flamme
  pinMode(capt_num, INPUT);// Définir l'interface du capteur de flamme en entrée
  pinMode(capt_ana, INPUT);// Définir l'interface du capteur de flamme en entrée
  Serial.begin(9600);

  
  //ultrason
  pinMode(pinTrig, OUTPUT);
  pinMode(pinEcho, INPUT);
  digitalWrite(pinTrig, LOW);  
  Serial.begin(9600);

   //ultrason2
  pinMode(pinTrig2, OUTPUT);
  pinMode(pinEcho2, INPUT);
  digitalWrite(pinTrig2, LOW);  
  Serial.begin(9600);


  //lcd
  lcd.begin(16, 2);
  
  // LED output.
  pinMode(ledPin, OUTPUT);

//moteur
 servo_2.attach(23);
 
 //capteur de mouvement
 pinMode(MouvementPin, INPUT);     // declare sensor as input
  }

void loop() {


  //flamme
  capteur_flamme = analogRead(capt_ana);   
  val = digitalRead (capt_num) ;
    if (val == HIGH)
  {
    sendSMS("il y a un incendie en adr");
  }

  
  // ultrason
  digitalWrite(pinTrig, HIGH);        
  delayMicroseconds(10);
  digitalWrite(pinTrig, LOW);
  temps = pulseIn(pinEcho, HIGH);
  
  temps = temps/2;
    distance = (temps*340)/10000.0; // distance en cm deja calculee
    if(distance<5) // donc el poubelle pleine
    {
      lcd.clear();
      lcd.print("poubelle pleine");
      //GSM --> update lel base de donnnees
      
    }
    else
    {
      lcd.clear();
      lcd.print("poubelle vide");
      //GSM --> update lel base de donnnees
      
    }
 
  //ultrason2
  digitalWrite(pinTrig2, HIGH);        
  delayMicroseconds(10);
  digitalWrite(pinTrig2, LOW);
  temps2 = pulseIn(pinEcho2, HIGH);
  
  temps2 = temps2/2;
    distance2 = (temps2*340)/10000.0; // distance en cm
   if(distance2<20)
    {
   
   servo_2.write(120);
   delay(4000);
   trashState=1;
   

   lightVal = analogRead(lightsensorPin);
   if (lightVal < 250)
   {
    digitalWrite(ledPin, HIGH);
  
  newDistance = 1;
  while (newDistance < 10) {
    digitalWrite(pinTrig2, HIGH);        
    delayMicroseconds(10);
    digitalWrite(pinTrig2, LOW);
    temps2 = pulseIn(pinEcho2, HIGH);
    temps2 = temps2/2;
    newDistance = (temps2*340)/10000.0;
  }
   servo_2.write(0);
   delay(1500);
     digitalWrite(ledPin, LOW); //
   }
   else
   {

   newDistance = 1;
   while (newDistance < 10) {
    digitalWrite(pinTrig2, HIGH);        
    delayMicroseconds(10);
    digitalWrite(pinTrig2, LOW);
    temps2 = pulseIn(pinEcho2, HIGH);
    temps2 = temps2/2;
    newDistance = (temps2*340)/10000.0;
  }
     servo_2.write(0);
     delay(1500);
   }
 }
 
 
 //***********************
 //** code pour detecter s'il y a un mouvement dans la poubelle (animaux, etc ..) **
 // **********************

 
 if(trashState==1)
 {
  trashState=0;
  old_time_for_5min= millis(); 
  old_time_for_20sec= millis();
 }
 else
 {
   new_time=millis();
   difference=new_time-old_time_for_20sec;
   if(difference>20000)
   {
    if (digitalRead(MouvementPin) == HIGH)
    {
      difference=new_time-old_time_for_5min;
      if(difference>300000)
      {

    sendSMS("Il y'a  un être vivant coinsé dans une poubelle à cette adresse + adr ");
        old_time_for_5min= millis();
        old_time_for_20sec= millis();
      }
      else 
      {
        old_time_for_20sec= millis();
      }
    }
    else
    {
      old_time_for_5min= millis();
      old_time_for_20sec= millis();
    }
     
   }
   
 }
 
 //loop end
 }
 
void sendSMS(String msgToBeSent) {
  SIM900.print("AT+CMGF=1\r");
  delay(100);
  SIM900.println("AT + CMGS = \"+216++++++++\"");
  delay(100);
  SIM900.println(msgToBeSent);
  delay(100);
  SIM900.println((char)26);
  delay(100);
  SIM900.println();
  delay(5000);
}
 